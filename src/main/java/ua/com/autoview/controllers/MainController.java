package ua.com.autoview.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.com.autoview.domains.MainEntity;
import ua.com.autoview.services.MainService;

import java.util.List;

/**
 * Created by java on 14.06.2015.
 */

@Controller
public class MainController {

    @Autowired
    MainService mainService;
    @RequestMapping("/main")
    public List<MainEntity> getMainEntity() {
        return mainService.getMainEntity();
    }


}
