package ua.com.autoview.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.com.autoview.domains.MainEntity;
import ua.com.autoview.repositiries.MainDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by java on 14.06.2015.
 */
@Service
public class MainService {
    @Autowired
    MainDAO mainDAO;

    public List<MainEntity> getMainEntity() {
        return mainDAO.getMainEntity();
    }
}
